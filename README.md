Cross-Platform SIG
=======
UP Association for Computing Machineries
* Target Time Each Meeting: 1 hour
* Venue: TLC/Serials Discussion Rom
* Heads/Speakers:
  1. Christiane Joy Yee (Joy Tan)
  2. Gian Carlo Sagun

Installation
======
Reference: [www.meteor.com/install](https://www.meteor.com/install)

- Linux or OS X
   
    Type this in the terminal:
   ```
   curl https://install.meteor.com/ | sh
   ```

- Windows

    Click [here](https://install.meteor.com/windows)

Meeting 1
------
Date: February 4, 2016
1. Hello World on the [meteor](www.meteor.com) environment.
2. Creating an App
3. Templates
